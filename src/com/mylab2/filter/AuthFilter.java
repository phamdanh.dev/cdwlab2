package com.mylab2.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mylab2.util.URLConstants;

@WebFilter(urlPatterns = "/*")
public class AuthFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		String action = req.getServletPath();
		if (action.equals(URLConstants.URL_LOGIN) || action.equals("/loginajax")) {
			chain.doFilter(request, response);
			return;
		}
		HttpSession session = req.getSession();
		if (session == null || session.getAttribute("USER_LOGIN") == null) {
			resp.sendRedirect(req.getContextPath() + URLConstants.URL_LOGIN);
			return;
		}
		chain.doFilter(request, response);
		
	}

}
