package com.mylab2.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConnection {

	public static Connection getConnection() {

		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/simpleforum?useUnicode=true&characterEncoding=utf8", "root", "Danh1998");
			return conn;
		} catch (ClassNotFoundException e) {
			System.out.println("KHONG TIM THAY DRIVER DATABASE");
			e.printStackTrace();
		} catch (SQLException e) {
			System.out.println("SAI THONG TIN DATABASE");
			e.printStackTrace();
		}
		return null;

	}
}
