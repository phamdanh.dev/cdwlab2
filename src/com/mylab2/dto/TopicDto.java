package com.mylab2.dto;

public class TopicDto {
	private int userId;
	private int topicId;
	private String username;
	private String dateRegister;
	private String datetime;
	private String topicHeader;
	private int numberReply;
	
	public TopicDto() {
		
	}

	public TopicDto(int userId, int topicId, String username, String dateRegister, String datetime, String topicHeader,
			int numberReply) {
		super();
		this.userId = userId;
		this.topicId = topicId;
		this.username = username;
		this.dateRegister = dateRegister;
		this.datetime = datetime;
		this.topicHeader = topicHeader;
		this.numberReply = numberReply;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDateRegister() {
		return dateRegister;
	}

	public void setDateRegister(String dateRegister) {
		this.dateRegister = dateRegister;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getTopicHeader() {
		return topicHeader;
	}

	public void setTopicHeader(String topicHeader) {
		this.topicHeader = topicHeader;
	}

	public int getNumberReply() {
		return numberReply;
	}

	public void setNumberReply(int numberReply) {
		this.numberReply = numberReply;
	}

}
