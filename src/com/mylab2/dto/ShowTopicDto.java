package com.mylab2.dto;

public class ShowTopicDto {
	private int topicId;
	private int userId;
	private String username;
	private String dateRegister;
	private String datetime;
	private String topicHeader;
	private String topicContent;
	
	public ShowTopicDto() {
	}

	public ShowTopicDto(int topicId, int userId, String username, String dateRegister, String datetime,
			String topicHeader, String topicContent) {
		super();
		this.topicId = topicId;
		this.userId = userId;
		this.username = username;
		this.dateRegister = dateRegister;
		this.datetime = datetime;
		this.topicHeader = topicHeader;
		this.topicContent = topicContent;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDateRegister() {
		return dateRegister;
	}

	public void setDateRegister(String dateRegister) {
		this.dateRegister = dateRegister;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getTopicHeader() {
		return topicHeader;
	}

	public void setTopicHeader(String topicHeader) {
		this.topicHeader = topicHeader;
	}

	public String getTopicContent() {
		return topicContent;
	}

	public void setTopicContent(String topicContent) {
		this.topicContent = topicContent;
	}
	
	
	
}
