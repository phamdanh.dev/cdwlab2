package com.mylab2.dto;

public class ReplyTopicDto {
	private int replyTopicId;
	private int topicId;
	private int userId;
	private String username;
	private String dateRegister;
	private String datetime;
	private String replyTopicContent;

	public ReplyTopicDto() {
	}

	public ReplyTopicDto(int replyTopicId, int topicId, int userId, String username, String dateRegister,
			String datetime, String replyTopicContent) {
		super();
		this.replyTopicId = replyTopicId;
		this.topicId = topicId;
		this.userId = userId;
		this.username = username;
		this.dateRegister = dateRegister;
		this.datetime = datetime;
		this.replyTopicContent = replyTopicContent;
	}

	public int getReplyTopicId() {
		return replyTopicId;
	}

	public void setReplyTopicId(int replyTopicId) {
		this.replyTopicId = replyTopicId;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDateRegister() {
		return dateRegister;
	}

	public void setDateRegister(String dateRegister) {
		this.dateRegister = dateRegister;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getReplyTopicContent() {
		return replyTopicContent;
	}

	public void setReplyTopicContent(String replyTopicContent) {
		this.replyTopicContent = replyTopicContent;
	}
	
	
	
}
