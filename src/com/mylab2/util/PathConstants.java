package com.mylab2.util;

public class PathConstants {
	
	public static final String PATH_LOGIN = "/WEB-INF/views/login.jsp";
	public static final String PATH_LIST_TOPICS = "/WEB-INF/views/listTopics.jsp";
	public static final String PATH_REPLY_TOPIC = "/WEB-INF/views/replyTopic.jsp";
	public static final String PATH_SHOW_TOPIC = "/WEB-INF/views/showTopic.jsp";
	public static final String PATH_NEW_TOPIC = "/WEB-INF/views/newTopic.jsp";

}
