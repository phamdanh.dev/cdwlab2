package com.mylab2.util;

public class URLConstants {
	
	public static final String URL_LOGIN = "/login";
	public static final String URL_LIST_TOPICS = "/list-topic";
	public static final String URL_SHOW_TOPIC = "/show-topic";
	public static final String URL_REPLY_TOPIC = "/reply-topic";
	public static final String URL_NEW_TOPIC = "/new-topic";
	public static final String URL_LOGOUT = "/logout";

}
