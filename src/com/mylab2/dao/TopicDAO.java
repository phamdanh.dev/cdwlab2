package com.mylab2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mylab2.connection.JDBCConnection;
import com.mylab2.dto.ReplyTopicDto;
import com.mylab2.dto.ShowTopicDto;
import com.mylab2.dto.TopicDto;
import com.mylab2.entity.ReplyTopic;
import com.mylab2.entity.Topic;

public class TopicDAO {

	public List<TopicDto> findAllTopic() {

		String query = "select user.id, user.username, user.dateRegister, topic.topicId, topic.datetime, topic.topicHeader, count(distinct replytopic.replytopicId) as numberReply from topic join user  on user.id = topic.userId left join replytopic on topic.topicId = replytopic.topicId group by topic.topicId order by topic.topicId;";

		List<TopicDto> topicDtos = new ArrayList<TopicDto>();

		try (Connection connection = JDBCConnection.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(query);
			System.out.println(statement);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				TopicDto topicDto = new TopicDto();
				topicDto.setUserId(rs.getInt("id"));
				topicDto.setTopicId(rs.getInt("topicId"));
				topicDto.setUsername(rs.getString("username"));
				topicDto.setDateRegister(rs.getString("dateRegister"));
				topicDto.setDatetime(rs.getString("datetime"));
				topicDto.setTopicHeader(rs.getString("topicHeader"));
				topicDto.setNumberReply(rs.getInt("numberReply"));
				topicDtos.add(topicDto);
			}

		} catch (Exception e) {
			System.out.println("LOI CAU TRUY VAN findAllTopic");
			e.printStackTrace();
		}
		return topicDtos;
	}

	public boolean insertTopic(Topic topic) {

		String query = "INSERT INTO topic(userId, datetime, topicHeader, topicContent) VALUES (?, ?, ?, ?)";
		try (Connection connection = JDBCConnection.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setInt(1, topic.getUserId());
			statement.setString(2, topic.getDatetime());
			statement.setString(3, topic.getTopicHeader());
			statement.setString(4, topic.getTopicContent());
			statement.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	public ShowTopicDto findTopicByTopicId(int topicId) {

		String query = "SELECT topic.topicId, topic.userId, topic.datetime, topic.topicHeader,topic.topicContent, user.username, user.dateRegister FROM topic JOIN user ON topic.userId = user.id WHERE topicId = ?";
		ShowTopicDto showTopicDto = new ShowTopicDto();

		try (Connection connection = JDBCConnection.getConnection()) {

			PreparedStatement statement = connection.prepareStatement(query);
			statement.setInt(1, topicId);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {

				showTopicDto.setTopicId(rs.getInt("topicId"));
				showTopicDto.setUserId(rs.getInt("userId"));
				showTopicDto.setUsername(rs.getString("username"));
				showTopicDto.setDateRegister(rs.getString("dateRegister"));
				showTopicDto.setDatetime(rs.getString("datetime"));
				showTopicDto.setTopicHeader(rs.getString("topicHeader"));
				showTopicDto.setTopicContent(rs.getString("topicContent"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return showTopicDto;
	}

	public List<ReplyTopicDto> findReplyTopicByTopicId(int topicId) {

		String query = "SELECT replytopic.replyTopicId, replytopic.userId, replytopic.topicId, replytopic.datetime, replytopic.replyTopicContent, user.username, user.dateRegister FROM replytopic JOIN user ON replytopic.userId = user.id WHERE topicId = ?;";
		
		List<ReplyTopicDto> replyTopicDtos = new ArrayList<ReplyTopicDto>();

		try (Connection connection = JDBCConnection.getConnection()) {

			PreparedStatement statement = connection.prepareStatement(query);
			statement.setInt(1, topicId);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				ReplyTopicDto replyTopicDto = new ReplyTopicDto();
				replyTopicDto.setReplyTopicId(rs.getInt("replyTopicId"));
				replyTopicDto.setUserId(rs.getInt("userId"));
				replyTopicDto.setTopicId(rs.getInt("topicId"));
				replyTopicDto.setUsername(rs.getString("username"));
				replyTopicDto.setDateRegister(rs.getString("dateRegister"));
				replyTopicDto.setDatetime(rs.getString("datetime"));
				replyTopicDto.setReplyTopicContent(rs.getString("replyTopicContent"));
				replyTopicDtos.add(replyTopicDto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return replyTopicDtos;
	}

	public boolean insertReplyTopic(ReplyTopic replyTopic) {
		String query = "INSERT INTO replytopic(userId, topicId, datetime,replyTopicContent) VALUES (?, ?, ?, ?)";
		try (Connection connection = JDBCConnection.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(query);
			statement.setInt(1, replyTopic.getUserId());
			statement.setInt(2, replyTopic.getTopicId());
			statement.setString(3, replyTopic.getDatetime());
			statement.setString(4, replyTopic.getReplyTopicContent());
			statement.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
		
	}

}
