package com.mylab2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mylab2.connection.JDBCConnection;
import com.mylab2.dto.TopicDto;
import com.mylab2.entity.User;

public class UserDAO {

	public User findUserByUsername(String username) {

		String query = "SELECT * FROM user WHERE username = ?";
		User user = null;

		try (Connection conn = JDBCConnection.getConnection()) {

			PreparedStatement statement = conn.prepareStatement(query);
			statement.setString(1, username);
			System.out.println(statement);
			ResultSet rs = statement.executeQuery();

			while (rs.next()) {
				user = new User();
				user.setId(rs.getInt("id"));
				user.setUsername(rs.getString("username"));
				user.setPassword(rs.getString("password"));
				break;
			}

		} catch (SQLException e) {
			System.out.println("Khong tim thay User By Email");
			e.printStackTrace();
		}

		return user;

	}

	

}
