package com.mylab2.entity;

public class ReplyTopic {
	
	private int id;
	private int userId;
	private int topicId;
	private String datetime;
	private String replyTopicContent;
	
	public ReplyTopic() {
		// TODO Auto-generated constructor stub
	}

	public ReplyTopic(int userId, int topicId, String datetime, String replyTopicContent) {
		super();
		this.userId = userId;
		this.topicId = topicId;
		this.datetime = datetime;
		this.replyTopicContent = replyTopicContent;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getTopicId() {
		return topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getReplyTopicContent() {
		return replyTopicContent;
	}

	public void setReplyTopicContent(String replyTopicContent) {
		this.replyTopicContent = replyTopicContent;
	}

	public int getId() {
		return id;
	}
	
	

}
