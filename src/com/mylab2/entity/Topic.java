package com.mylab2.entity;

public class Topic {
	
	private int id;
	private int userId;
	private String datetime;
	private String topicHeader;
	private String topicContent;
	
	public Topic() {
		// TODO Auto-generated constructor stub
	}

	public Topic(int userId, String datetime, String topicHeader, String topicContent) {
		super();
		this.userId = userId;
		this.datetime = datetime;
		this.topicHeader = topicHeader;
		this.topicContent = topicContent;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getDatetime() {
		return datetime;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getTopicHeader() {
		return topicHeader;
	}

	public void setTopicHeader(String topicHeader) {
		this.topicHeader = topicHeader;
	}

	public String getTopicContent() {
		return topicContent;
	}

	public void setTopicContent(String topicContent) {
		this.topicContent = topicContent;
	}

	public int getId() {
		return id;
	}
	
	

}
