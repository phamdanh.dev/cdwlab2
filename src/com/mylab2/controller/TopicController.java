package com.mylab2.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mylab2.dao.TopicDAO;
import com.mylab2.dto.ReplyTopicDto;
import com.mylab2.dto.ShowTopicDto;
import com.mylab2.dto.TopicDto;
import com.mylab2.entity.ReplyTopic;
import com.mylab2.entity.Topic;
import com.mylab2.util.PathConstants;
import com.mylab2.util.URLConstants;


@WebServlet(urlPatterns = { URLConstants.URL_NEW_TOPIC, URLConstants.URL_LIST_TOPICS, URLConstants.URL_REPLY_TOPIC,
		URLConstants.URL_SHOW_TOPIC })
public class TopicController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TopicDAO topicDAO;
	private Topic topic;
	private ReplyTopic replyTopic;
	private ShowTopicDto showTopicDto;
	private List<ReplyTopicDto> replyTopicDtos;
	HttpSession session;
	SimpleDateFormat simpleDateFormat;
	@Override
	public void init() throws ServletException {
		topicDAO = new TopicDAO();
		simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm aa");
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String action = req.getServletPath();

		switch (action) {
		case URLConstants.URL_NEW_TOPIC :
			req.getRequestDispatcher(PathConstants.PATH_NEW_TOPIC).forward(req, resp);
			break;
		case URLConstants.URL_LIST_TOPICS:
			getTopicList(req, resp);
			break;
		case URLConstants.URL_REPLY_TOPIC:
			getReplyTopic(req, resp);
			break;
		case URLConstants.URL_SHOW_TOPIC:
			getTopic(req, resp);
			break;

		default:
			break;
		}
	}

	private void getTopic(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int topicId = Integer.parseInt(req.getParameter("topicId"));
		showTopicDto = topicDAO.findTopicByTopicId(topicId);
		replyTopicDtos = topicDAO.findReplyTopicByTopicId(topicId);
		req.setAttribute("topic", showTopicDto);
		req.setAttribute("replyTopics", replyTopicDtos);
		req.getRequestDispatcher(PathConstants.PATH_SHOW_TOPIC).forward(req, resp);
		
	}

	private void getReplyTopic(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int topicId = Integer.parseInt(req.getParameter("topicId"));
		showTopicDto = topicDAO.findTopicByTopicId(topicId);
		session = req.getSession();
		session.setAttribute("topicId", topicId);
		req.setAttribute("topic", showTopicDto);
		req.getRequestDispatcher(PathConstants.PATH_REPLY_TOPIC).forward(req, resp);
		
	}

	private void getTopicList(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<TopicDto> topicDtos = topicDAO.findAllTopic();
		req.setAttribute("topicDtos", topicDtos);
		req.getRequestDispatcher(PathConstants.PATH_LIST_TOPICS).forward(req, resp);
		
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String action = req.getServletPath();
		
		switch (action) {
		case URLConstants.URL_NEW_TOPIC:
			postNewTopic(req, resp);
			break;
			
		case URLConstants.URL_SHOW_TOPIC:
			postReplyTopic(req, resp);
			break;
		}
		
	}

	private void postReplyTopic(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		int userId = (int) session.getAttribute("userId");
		int topicId = (int) session.getAttribute("topicId");
		String replyTopicContent = req.getParameter("replyTopicContent");
		replyTopic = new ReplyTopic();
		replyTopic.setUserId(userId);
		replyTopic.setTopicId(topicId);
		replyTopic.setReplyTopicContent(replyTopicContent);
		replyTopic.setDatetime(simpleDateFormat.format(new Date()).toString());
		topicDAO.insertReplyTopic(replyTopic);
		resp.sendRedirect(req.getContextPath() + URLConstants.URL_SHOW_TOPIC + "?topicId=" + topicId);
		session.removeAttribute("topicId");
	}

	private void postNewTopic(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		session = req.getSession();
		String topicHeader = req.getParameter("topicHeader");
		String topicContent = req.getParameter("topicContent");
		
		
		int userId = (int) session.getAttribute("userId");
		
		topic = new Topic();
		topic.setUserId(userId);
		topic.setDatetime(simpleDateFormat.format(new Date()).toString());
		topic.setTopicHeader(topicHeader);
		topic.setTopicContent(topicContent);
		topicDAO.insertTopic(topic);
		resp.sendRedirect(req.getContextPath() + URLConstants.URL_LIST_TOPICS);
	}

}
