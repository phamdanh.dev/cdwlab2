package com.mylab2.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.mindrot.jbcrypt.BCrypt;

import com.mylab2.dao.UserDAO;
import com.mylab2.entity.User;
import com.mylab2.util.URLConstants;

@WebServlet(urlPatterns = { URLConstants.URL_LOGIN, URLConstants.URL_LOGOUT, "/loginajax" })
public class AuthController extends HttpServlet {

	private UserDAO userDAO = null;
	private User user = null;
	private HttpSession session;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		userDAO = new UserDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		System.out.println(action);
		PrintWriter out = resp.getWriter();
		session = req.getSession();
		switch (action) {
		case URLConstants.URL_LOGIN:
			req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, resp);
			break;

		case "/loginajax":

			if (req.getParameter("username") != null) {
				String usernameAjax = req.getParameter("username");
				System.out.println(usernameAjax);
				user = userDAO.findUserByUsername(usernameAjax);
				if (user != null) {
					System.out.println("DA CHUI DUOC VAO TRONG DAY");
					String msg = "Username valid!";
					out.write(msg);
				}
				if (user == null) {
					System.out.println("DA CHUI DUOC VAO TRONG DAY");
					String msg = "Warning: Username Invalid!";
					out.write(msg);
				}
				break;
			}
	

		case URLConstants.URL_LOGOUT:
			if (session != null || session.getAttribute("USER_LOGIN") != null) {
				session.removeAttribute("USER_LOGIN");
				session.removeAttribute("userId");
			}
			resp.sendRedirect(req.getContextPath() + URLConstants.URL_LOGIN);
			break;

		case "/login-ajax":

		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getServletPath();
		switch (action) {
		case URLConstants.URL_LOGIN:
			String username = req.getParameter("username");
			String password = req.getParameter("password");
			user = userDAO.findUserByUsername(username);
			req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, resp);
			if (user == null || !user.getPassword().equals(password)) {
				req.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(req, resp);
				return;
			} else {
				HttpSession session = req.getSession();
				session.setAttribute("USER_LOGIN", user.getUsername());
				session.setAttribute("userId", user.getId());
				resp.sendRedirect(req.getContextPath() + "/list-topic");

			}

		}
	}

}
