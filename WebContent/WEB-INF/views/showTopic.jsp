<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.mylab2.util.URLConstants"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>SHOW TOPIC PAGE</title>

<style type="text/css">
.div {
	font-family: sans-serif;
}
</style>

</head>
<body>
	<div
		style="text-align: right; background-color: indigo; color: white; height: 50px">
		<a href='<c:url value='${ URLConstants.URL_LOGOUT }'></c:url>'
			class="btn btn-danger" role="button" aria-pressed="true"
			style="margin-top: 6px; margin-right: 20px; float: right;">Đăng
			xuất</a>
		<p
			style="font-family: sans-serif; margin-top: 15px; margin-right: 20px; float: right;">
			<c:out value='${sessionScope.USER_LOGIN}' />
		</p>
	</div>
	<div class="container">
		<div class="row" style="margin-top: 50px; margin-bottom: 30px">
			<h3>Chủ đề: ${ topic.topicHeader }</h3>
		</div>


		<div class="row" style="margin-bottom: 20px">
			<a href='<c:url value='${ URLConstants.URL_LIST_TOPICS }'></c:url>'
				class="btn btn-success" role="button" aria-pressed="true"
				style="margin-left: 940px; width: 200px">Danh sách chủ đề</a>
		</div>


		<div class="row"
			style="width: -webkit-fill-available; margin-bottom: 20px;">
			<div style="width: -webkit-fill-available"">
				<div class="row"
					style="width: -webkit-fill-available; padding-left: 10px; border: solid 2px indigo; border-radius: 10px 10px 0px 0px; background-color: indigo; color: white;">
					<span>${ topic.datetime }</span>
				</div>
				<div class="row" style="width: -webkit-fill-available;">
					<div class="col-2"
						style="display: flex; justify-content: center; align-items: center; height: auto; border-left: solid 2px indigo; border-bottom: solid 2px indigo; border-radius: 0px 0px 0px 10px; border-right: solid 2px indigo; text-align: center;">
						<div>
							<span>${ topic.username }</span><br> <span>Tham gia
								${ topic.dateRegister }</span>
						</div>
					</div>
					<div class="col-10"
						style="border-right: solid 2px indigo; border-bottom: solid 2px indigo; border-radius: 0px 0px 10px 0px;">
						<div class="row"
							style="border-bottom: solid 2px indigo; padding: 10px;">
							<div>
								<span>${ topic.topicHeader }</span>
							</div>
						</div>
						<div class="row" style="padding: 10px;">
							<div>
								<span>${ topic.topicContent }</span>
							</div>
						</div>
						<div class="row"">
							<div style="margin-left: 850px;">
								<a
									href='<c:url value='${ URLConstants.URL_REPLY_TOPIC }?topicId=${ topic.topicId }'></c:url>'
									class="btn btn-sm btn-primary" role="button"
									aria-pressed="true" style="margin: 5px;">Trả lời</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<c:forEach varStatus="loop" var="item" items="${ replyTopics }">
			<div class="row"
				style="width: -webkit-fill-available; margin-bottom: 20px;">
				<div style="width: -webkit-fill-available">
					<div class="row"
						style="width: -webkit-fill-available; padding-left: 10px; border: solid 2px indigo; border-radius: 10px 10px 0px 0px; background-color: indigo; color: white;">
						<span>${ item.datetime }</span>
					</div>
					<div class="row" style="width: -webkit-fill-available;">
						<div class="col-2"
							style="display: flex; justify-content: center; align-items: center; height: auto; border-left: solid 2px indigo; border-bottom: solid 2px indigo; border-radius: 0px 0px 0px 10px; border-right: solid 2px indigo; text-align: center;">
							<div>
								<span>${ item.username }</span><br> <span>Tham gia ${ item.dateRegister }</span>
							</div>
						</div>
						<div class="col-10"
							style="border-right: solid 2px indigo; border-bottom: solid 2px indigo; border-radius: 0px 0px 10px 0px;">
							<div class="row"
								style="border-bottom: solid 2px indigo; padding: 10px;">
								<div>
									<span>Re: ${ topic.topicHeader }</span>
								</div>
							</div>
							<div class="row" style="padding: 10px;">
								<div>
									<span>${ item.replyTopicContent }</span>
								</div>
							</div>
							<div class="row"">
								<div style="margin-left: 850px;">
									<a
										href='<c:url value='${ URLConstants.URL_REPLY_TOPIC }?topicId=${ topic.topicId }'></c:url>'
										class="btn btn-sm btn-primary" role="button"
										aria-pressed="true" style="margin: 5px;">Trả lời</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>

		<div class="row" style="margin-bottom: 20px">
			<a href="" class="btn" role="button" aria-pressed="true"
				style="margin-left: 940px; width: 200px; background-color: indigo; color: white;">Về
				đầu trang</a>
		</div>
	</div>
</body>
<!-- CSS only -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</html>