<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.mylab2.util.URLConstants"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>NEW TOPIC PAGE</title>

<style type="text/css">
.div {
	font-family: sans-serif;
}
</style>

</head>


<body>
	<div
		style="text-align: right; background-color: indigo; color: white; height: 50px">
		<a href='<c:url value='${ URLConstants.URL_LOGOUT }'></c:url>'
			class="btn btn-danger" role="button" aria-pressed="true"
			style="margin-top: 6px; margin-right: 20px; float: right;">Đăng
			xuất</a>
		<p
			style="font-family: sans-serif; margin-top: 15px; margin-right: 20px; float: right;">
			<c:out value='${sessionScope.USER_LOGIN}' />
		</p>
	</div>
	<div class="container">
		<div class="row" style="margin-top: 30px; margin-bottom: 20px">
			<h2>Tạo chủ đề mới</h2>
		</div>
		<div class="row">
			<form action='<c:url value='${ URLConstants.URL_NEW_TOPIC }'></c:url>' method="POST" style="width: 800px">
				<div class="form-group">
					<h5>Tiêu đề</h5>
					<input type="text" class="form-control" id="topicHeader"
						name="topicHeader">
				</div>
				<div class="form-group">
					<h5>Nội dung</h5>
					<textarea class="form-control" id="topicContent"
						name="topicContent" rows="5" style="overflow-y: scroll;"></textarea>
				</div>
				<div style="text-align: right; margin-top: 30px">
					<a href='<c:url value='${ URLConstants.URL_LIST_TOPICS }'></c:url>'
						class="btn btn-danger" role="button" aria-pressed="true"
						style="width: 60px">Hủy</a>
					<button type="submit" class="btn btn-success" style="width: 200px">Gửi</button>
				</div>
			</form>
		</div>
	</div>
</body>
<!-- CSS only -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</html>