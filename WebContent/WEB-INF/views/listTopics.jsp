<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.mylab2.util.URLConstants"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>LIST TOPICS PAGE</title>

<style>
.div {
	font-family: sans-serif;
}

.tdRepNum {
	text-align: center;
	padding: 24px !important;
}

.ptable {
	margin-bottom: 0px;
}
</style>

</head>

<body>
	<div
		style="text-align: right; background-color: indigo; color: white; height: 50px">
		<a href='<c:url value='${ URLConstants.URL_LOGOUT }'></c:url>'
			class="btn btn-danger" role="button" aria-pressed="true"
			style="margin-top: 6px; margin-right: 20px; float: right;">Đăng
			xuất</a>
		<p
			style="font-family: sans-serif; margin-top: 15px; margin-right: 20px; float: right;">
			<c:out value='${sessionScope.USER_LOGIN}' />
		</p>
	</div>
	<div class="container">
		<div class="row" style="margin-top: 50px; margin-bottom: 30px">
			<h3>Diễn đàn: Chuyện học phí và các chính sách hỗ trợ học tập</h3>
		</div>
		<div class="row" style="margin-bottom: 20px">
			<a href='<c:url value='${ URLConstants.URL_NEW_TOPIC }'></c:url>'
				class="btn btn-success" role="button" aria-pressed="true"
				style="margin-left: 20px; width: 200px">THÊM CHỦ ĐỀ MỚI</a>
		</div>
		<div class="row">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th scope="col"
							style="width: 1000px; text-align: center; padding: 24px">Chủ
							đề</th>
						<th scope="col" class="tdRepNum">Phản hồi</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${ topicDtos }" var="item" varStatus="loop">
						<tr>
							<td>
								<p class="ptable">
									<a href='<c:url value='${ URLConstants.URL_SHOW_TOPIC}?topicId=${ item.topicId }'></c:url>'>${item.topicHeader}</a>
								</p>
								<p class="ptable">
									<span>Bài mới nhất bởi ${ item.username }, </span> <span>${ item.datetime }</span>
								</p>

							</td>
							<td class="tdRepNum"><span>${ item.numberReply }</span></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
<!-- CSS only -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
</html>